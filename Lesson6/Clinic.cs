﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    //4.Создать программу для имитации работы клиники.
    //Пусть в клинике будет три врача: хирург, терапевт и дантист.
    //Каждый врач имеет метод «лечить», но каждый врач лечит по - своему.Так же предусмотреть класс «Пациент» и класс «План лечения».
    //Создать объект класса «Пациент» и добавить пациенту план лечения.Так же создать метод,
    //который будет назначать врача пациенту согласно плану лечения.
    //Если план лечения имеет код 1 – назначить хирурга и выполнить метод лечить.
    //Если план лечения имеет код 2 – назначить дантиста и выполнить метод лечить.
    //Если план лечения имеет любой другой код – назначить терапевта и выполнить метод лечить.

    //Наколхозил, не очень нравится своё решение, немного не хватило времени =)

    public static class Surgeon 
    {
        public static int obligation { get; } = 1;
        public static string cure()
        {
            return "the surgeon performs the operation";
        }
    }

    public static class Therapist
    {
        public static int obligation { get; } = 2;
        public static string cure()
        {
            return "prescribing medication by a therapist";
        }
    }

    public static class Dentist
    {
        public static int obligation { get; } = 3;
        public static string cure()
        {
            return "dentist treatment";
        }
    }

    public static class TreatmentPlan
    {
        public static Dictionary<int, string> treatmentPlan = new Dictionary<int, string>()
        {
            [1] = Surgeon.cure(),
            [2] = Therapist.cure(),
            [3] = Dentist.cure(),
        };
    }

    public class Patient
    {
        public int treatmentPlan = default;
        public Patient()
        {
            Random rnd = new Random();
            treatmentPlan = rnd.Next(1, 3);
        }
    }
}

