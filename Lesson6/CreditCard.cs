﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    //2.Создать класс CreditCard c полями номер счета, текущая сумма на счету.
    //Добавьте метод, который позволяет начислять сумму на кредитную
    //карточку.
    //Добавьте метод, который позволяет снимать с карточки некоторую
    //сумму.
    //Добавьте метод, который выводит текущую информацию о карточке.
    //Напишите программу, которая создает три объекта класса CreditCard у
    //которых заданы номер счета и начальная сумма
    //Тестовый сценарий для проверки:
    //Положите деньги на первые две карточки и снимите с третьей.
    //Выведите на экран текущее состояние всех трех карточек.

    internal class CreditCard
    {
        private string accountNumber = default;
        private int balance = default;

        public CreditCard(string accountNumber, int balance)
        {
            this.accountNumber = accountNumber;
            if (balance > 0) { this.balance = balance; }            
        }

        public void putTheAmount(int amount)
        {
            balance += amount; 
        }

        public void getTheAmount(int amount)
        {
            if (amount <= balance) 
            {  balance =- amount; }
            else { Console.WriteLine("Insufficient funds in the account"); }
        }

        public void accountInfo()
        {
            Console.WriteLine($"Account number: {accountNumber}\nBalance: {balance}");
        }
    }
}
