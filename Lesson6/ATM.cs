﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    internal class ATM
    {
        //3.Создать класс, описывающий банкомат.
        //Набор купюр, находящихся в банкомате, должен задаваться тремя
        //свойствами:
        //количеством купюр номиналом 20, 50 и 100.
        //Сделать метод для добавления денег в банкомат.
        //Сделать метод, снимающуий деньги, который принимает сумму денег,
        //а возвращает булевое значение - успешность выполнения операции.
        //При снятии денег метод должен распечатывать каким количеством
        //купюр какого номинала выдаётся сумма.
        //Создать конструктор с тремя параметрами - количеством купюр каждого
        //номинала.

        private int countBill20 = default;
        private int countBill50 = default;
        private int countBill100 = default;

        public ATM(int countBill20 = 0, int countBill50 = 0, int countBill100 = 0)
        {
            this.countBill20 = countBill20;
            this.countBill50 = countBill50;
            this.countBill100 = countBill100;
        }

        public void putAmount(int countBill20 = 0, int countBill50 = 0, int countBill100 = 0)
        {
            this.countBill20 += countBill20;
            this.countBill50 += countBill50;
            this.countBill100 += countBill100;
        }

        public bool getAmount(int amount)
        {
            if (countBill20 * 20 + countBill50 * 50 + countBill100 * 100 >= amount)
            {
                int getCountBill100 = default, getCountBill50 = default, getCountBill20 = default;

                getCountBill100 = amount / 100;
                countBill100 -= getCountBill100;
                amount %= 100;

                getCountBill50 = amount / 50;
                countBill50 -= getCountBill50;
                amount %= 50;

                getCountBill20 = amount / 20;
                countBill20 -= getCountBill20;

                Console.WriteLine($"Issued money: 20Р = {getCountBill20}, 50Р = {getCountBill50}, 100Р = {getCountBill100}");

                return true;
            }
            else
            {
                Console.WriteLine($"Not enough money");
                return false;
            }
        }
    }
}
