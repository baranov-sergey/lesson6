﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson6
{
    //1.Класс Phone.
    //Создайте класс Phone, который содержит переменные number, model и weight.
    //Создайте три экземпляра этого класса.
    //Выведите на консоль значения их переменных.
    //Добавить в класс Phone методы: receiveCall, имеет один параметр – имя звонящего. Выводит на консоль сообщение “Звонит { name}”.
    //getNumber – возвращает номер телефона.Вызвать эти методы для каждого из объектов.
    //Добавить конструктор в класс Phone,
    //который принимает на вход три параметра для инициализации переменных класса -number, model и weight.
    //Добавить конструктор, который принимает на вход два параметра для инициализации переменных класса - number, model.
    //Добавить конструктор без параметров.
    //Вызвать из конструктора с тремя параметрами конструктор с двумя. Добавьте перегруженный метод receiveCall,
    //который принимает два параметра - имя звонящего и номер телефона звонящего.Вызвать этот метод.
    //Создать метод sendMessage.Данный метод принимает на вход номера телефонов, которым будет отправлено сообщение.
    //Метод выводит на консоль номера этих телефонов.

    internal class Phone
    {
        public long number;
        public string model;
        public int weight;

        public Phone()
        {
            long number = default;
            string model = string.Empty;
            int weight = default;
        }

        public Phone(long number, string model)
        {
            this.number = number;
            this.model = model;
        }

        public Phone(long number, string model, int weight) : this(number, model)
        {
            this.weight = weight;
        }

        public void receiveCall(string name)
        {
            Console.WriteLine($"Звонит {name}.");
        }

        public void receiveCall(string name, long number)
        {
            Console.WriteLine($"Звонит {name}: {number}.");
        }

        public long getNumber()
        {
            return number;
        }

        public static void sendMessage(Phone[] numbers)
        {
            foreach (var number in numbers)
            {
                Console.WriteLine(number.number);
            }
        }
    }
}
