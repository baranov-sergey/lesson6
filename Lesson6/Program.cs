﻿using Lesson6;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml.Linq;

internal class Program
{
    private static void Main(string[] args)
    {
        //Task1
        Phone phone1 = new Phone(84325678798, "iphone 10", 210);
        Phone phone2 = new Phone(86546789800, "Samsung s9", 182);
        Phone phone3 = new Phone(89994447856, "Pixel 7", 165);
        Phone[] phones = { phone1, phone2, phone3 };
        DisplayOutput(phones);

        long phoneNumber1 = phone1.getNumber();
        phone1.receiveCall("Petya");
        phone1.receiveCall("Petya", 82347658798);
        long phoneNumber2 = phone2.getNumber();
        phone2.receiveCall("Vasya");
        phone1.receiveCall("Vasya", 84323214565);
        long phoneNumber3 = phone3.getNumber();
        phone3.receiveCall("Senya");
        phone1.receiveCall("Senya", 88754379476);

        Phone.sendMessage(phones);

        //Task2
        CreditCard cc1 = new CreditCard("1234-3456-5675-3214", 100);
        CreditCard cc2 = new CreditCard("3456-8765-2345-9876", 250);
        CreditCard cc3 = new CreditCard("4796-3680-8489-3284", 500);

        cc1.putTheAmount(300);
        cc1.accountInfo();
        cc2.putTheAmount(450);
        cc2.accountInfo();
        cc3.getTheAmount(600);
        cc3.accountInfo();

        //Task4
        Patient patient = new Patient();
        appointADoctorToThePatient(patient);

        //Task5
        ATM invoice = new ATM(5,3,4);              //650
        invoice.putAmount(3,2,1);                  //+260 = 910
        bool result = invoice.getAmount(370);      //-370 = 
    }

    public static void appointADoctorToThePatient(Patient patient)
    {
        foreach (var plan in TreatmentPlan.treatmentPlan)
        {
            if (plan.Key == patient.treatmentPlan)
            {
                Console.WriteLine(plan.Value);
                break;
            }
        }
    }

    public static void DisplayOutput(Phone[] phones)
    {
        foreach (var phone in phones)
        {
            Console.WriteLine($"number: {phone.number}, model: {phone.model}, weight: {phone.weight}");
        }
    }
}